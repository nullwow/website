class EventsController extends Requests {
    
    constructor () {
        super()
    }

    doGetEvents = (cb) => {
        let type = 'GET';
        let path = 'events/';
        this.doRequest(type, path, null, cb)
    }
}

class Events extends EventsController {
    constructor() {
        super()
        // Initiate Events
        this.initiateEvents();
    }

    initiateEvents = () => {
        this.doGetEvents((err, evt)=>{
            if(err) {
                return doToastr('warning', 'API Error', 'Error load Eventos');
            }
            let votes = evt.votes;
            let eventos = evt.events;
            
            $('#eventsCurrentVotes').text(votes);

            if(votes > 1000) {
                this.setProgressBar(2000, votes);
            } else if (votes > 500) {
                this.setProgressBar(1000, votes);
            } else if (votes > 200) {
                this.setProgressBar(500, votes);
            } else if (votes > 100) {
                this.setProgressBar(200, votes);
            } else if (votes > 50) {
                this.setProgressBar(100, votes);
            } else if (votes > 20) {
                this.setProgressBar(50, votes);
            } else if (votes > 10) {
                this.setProgressBar(20, votes);
            } else {
                this.setProgressBar(10, votes);
            }

            eventos.forEach(element => {
                $('#eventosLista').append(
                    $($('<li>')
                        .text(element)
                        .append(
                            $('<i>')
                                .addClass('fa')
                                .addClass('fa-check')
                                .addClass('float-right')
                        )
                    )
                )
            });
            
        })
    }

    setProgressBar = (max, value) => {
        let curr = Math.floor((value * 100) / max);
        $('#eventProgressBar').width( curr + '%');
    
        $('#eventsMilestone').text(max);
    }
}